from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship
import csv
import requests

CSV_URL = "https://datahub.io/core/airport-codes/r/airport-codes.csv"
Base = declarative_base()


class Airport(Base):
    __tablename__ = "airports"

    id = Column('id', String(32), primary_key=True, unique=True)
    coord = Column('coordinates', String(64), unique=True)

    def __init__(self, id, coord):
        self.id = id
        self.coord = coord


engine = create_engine('mysql://root:root@127.0.0.1:3306/', echo=True)
engine.execute("create database if not exists airports")
engine = create_engine('mysql://root:root@127.0.0.1:3306/airports?use_unicode=1&charset=utf8',
                       echo=True) #utf8???
engine.execute("ALTER TABLE airports CONVERT TO CHARACTER SET utf8")

Base.metadata.create_all(bind=engine)
Session = sessionmaker(bind=engine)

session = Session()
with requests.Session() as s:
    download = s.get(CSV_URL)

    decoded_content = download.content.decode('utf8')

    cr = csv.reader(decoded_content.splitlines(), delimiter=',')
    my_list = list(cr)
    for row in my_list:
        session.merge(Airport(row[0], row[-1]))
session.commit()
airports = session.query(Airport).all()
for airport in airports:
    print(airport.id)
session.close()
